From: Cheyenne Wills <cwills@sinenomine.net>
Date: Thu, 27 Jan 2022 20:19:17 -0700
Subject: Linux-5.17: kernel func complete_and_exit renamed

Handle the Linux kernel function rename made in commit
 "exit: Rename complete_and_exit to kthread_complete_and_exit"
 (cead1855)

Add a new autoconf test for the linux function kthread_complete_and_exit
and if not found use a define to map kthread_complete_and_exit to
complete_and_exit.

Replace calls to complete_and_exit with kthread_complete_and_exit.

Change-Id: If8db2d0abb1de8b08f511e9ff67612ef605cd603
Reviewed-on: https://gerrit.openafs.org/14882
Tested-by: BuildBot <buildbot@rampaginggeek.com>
Reviewed-by: Andrew Deason <adeason@sinenomine.net>
Reviewed-by: Benjamin Kaduk <kaduk@mit.edu>
(cherry picked from commit a651d4db7f86a24ea6784f6f27d5c8482667267b)
---
 src/afs/LINUX/osi_compat.h  |  4 ++++
 src/afs/afs_call.c          | 18 ++++++++++--------
 src/cf/linux-kernel-func.m4 |  6 ++++++
 3 files changed, 20 insertions(+), 8 deletions(-)

diff --git a/src/afs/LINUX/osi_compat.h b/src/afs/LINUX/osi_compat.h
index 726b655..53a079b 100644
--- a/src/afs/LINUX/osi_compat.h
+++ b/src/afs/LINUX/osi_compat.h
@@ -27,6 +27,10 @@
 # endif
 #endif
 
+#if !defined(HAVE_LINUX_KTHREAD_COMPLETE_AND_EXIT)
+# define kthread_complete_and_exit complete_and_exit
+#endif
+
 #if defined(STRUCT_DENTRY_OPERATIONS_HAS_D_AUTOMOUNT) && !defined(DCACHE_NEED_AUTOMOUNT)
 # define DCACHE_NEED_AUTOMOUNT DMANAGED_AUTOMOUNT
 #endif
diff --git a/src/afs/afs_call.c b/src/afs/afs_call.c
index 32ad210..fab4c01 100644
--- a/src/afs/afs_call.c
+++ b/src/afs/afs_call.c
@@ -19,7 +19,9 @@
 #include "afs/afs_stats.h"
 #include "rx/rx_globals.h"
 #if !defined(UKERNEL)
-# if !defined(AFS_LINUX20_ENV)
+# if defined(AFS_LINUX20_ENV)
+#  include "osi_compat.h"
+# else
 #  include "net/if.h"
 #  ifdef AFS_SGI62_ENV
 #   include "h/hashing.h"
@@ -328,7 +330,7 @@ afsd_thread(void *rock)
 	sprintf(current->comm, "afs_callback");
 	afs_RXCallBackServer();
 	AFS_GUNLOCK();
-	complete_and_exit(0, 0);
+	kthread_complete_and_exit(0, 0);
 	break;
     case AFSOP_START_AFS:
 	sprintf(current->comm, "afs_afsstart");
@@ -342,7 +344,7 @@ afsd_thread(void *rock)
 	sprintf(current->comm, "afsd");
 	afs_Daemon();
 	AFS_GUNLOCK();
-	complete_and_exit(0, 0);
+	kthread_complete_and_exit(0, 0);
 	break;
     case AFSOP_START_BKG:
 #ifdef AFS_NEW_BKG
@@ -361,7 +363,7 @@ afsd_thread(void *rock)
 	afs_BackgroundDaemon();
 	AFS_GUNLOCK();
 #endif
-	complete_and_exit(0, 0);
+	kthread_complete_and_exit(0, 0);
 	break;
     case AFSOP_START_TRUNCDAEMON:
 	sprintf(current->comm, "afs_trimstart");
@@ -372,7 +374,7 @@ afsd_thread(void *rock)
 	sprintf(current->comm, "afs_cachetrim");
 	afs_CacheTruncateDaemon();
 	AFS_GUNLOCK();
-	complete_and_exit(0, 0);
+	kthread_complete_and_exit(0, 0);
 	break;
     case AFSOP_START_CS:
 	sprintf(current->comm, "afs_checkserver");
@@ -380,7 +382,7 @@ afsd_thread(void *rock)
 	complete(arg->complete);
 	afs_CheckServerDaemon();
 	AFS_GUNLOCK();
-	complete_and_exit(0, 0);
+	kthread_complete_and_exit(0, 0);
 	break;
     case AFSOP_RXEVENT_DAEMON:
 	sprintf(current->comm, "afs_evtstart");
@@ -398,7 +400,7 @@ afsd_thread(void *rock)
 	sprintf(current->comm, "afs_rxevent");
 	afs_rxevent_daemon();
 	AFS_GUNLOCK();
-	complete_and_exit(0, 0);
+	kthread_complete_and_exit(0, 0);
 	break;
 #ifdef RXK_LISTENER_ENV
     case AFSOP_RXLISTENER_DAEMON:
@@ -420,7 +422,7 @@ afsd_thread(void *rock)
 	sprintf(current->comm, "afs_rxlistener");
 	rxk_Listener();
 	AFS_GUNLOCK();
-	complete_and_exit(0, 0);
+	kthread_complete_and_exit(0, 0);
 	break;
 #endif
     default:
diff --git a/src/cf/linux-kernel-func.m4 b/src/cf/linux-kernel-func.m4
index 0ca3e44..cd4afe9 100644
--- a/src/cf/linux-kernel-func.m4
+++ b/src/cf/linux-kernel-func.m4
@@ -178,6 +178,12 @@ AC_CHECK_LINUX_FUNC([ip_sock_set],
                     [#include <net/ip.h>],
                     [ip_sock_set_mtu_discover(NULL, 0);])
 
+dnl Linux 5.17 renamed complete_and_exit to kthread_complete_and_exit
+AC_CHECK_LINUX_FUNC([kthread_complete_and_exit],
+                    [#include <linux/kernel.h>
+                     #include <linux/kthread.h>],
+                    [kthread_complete_and_exit(0, 0);])
+
 dnl Consequences - things which get set as a result of the
 dnl                above tests
 AS_IF([test "x$ac_cv_linux_func_d_alloc_anon" = "xno"],
