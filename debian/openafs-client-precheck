#! /bin/sh
# Copyright (C) 2014 by Benjamin Kaduk
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
PATH=/bin:/usr/bin:/sbin:/usr/sbin

# Gather up options and post startup script name, if present
if [ -f /etc/openafs/afs.conf ]; then
    . /etc/openafs/afs.conf
fi

CACHEINFO=${CACHEINFO:-/etc/openafs/cacheinfo}
MODULEROOT=${MODULEROOT:-/lib/modules/`uname -r`}

do_nothing_env() {
    cat > /var/cache/openafs-client/openafs-client.env <<EOF
AFSD_ARGS=-help
AFS_SETCRYPT=-help
AFS_SYSNAME=-help
KMOD=--version
EOF
}

# Exit if the package is not installed.
[ -x /sbin/afsd ] || exit 1

# Do some other checks for prerequisites
if ! [ -f "${CACHEINFO}" ]; then
    echo "required cacheinfo file does not exist" >&2
    exit 1
fi
if ! [ -d "$(awk -F : '{print $1}' < "${CACHEINFO}")" ]; then
    echo "AFS mountpoint is not a directory or does not exist" >&2
    exit 1
fi
if pidof /sbin/afsd || pidof /usr/sbin/afsd; then
    echo "afsd is already running, continuing" >&2
    do_nothing_env
    exit 0
fi

# Ensure that the kernel module is loaded.
if ! /sbin/lsmod | grep -Fq openafs; then
    modprobe openafs
    status=$?

    if [ $status -ne 0 ] ; then
	echo "Failed to load openafs.ko.  Does it need to be built?" >&2
	# We cannot fail hard on a missing module, though, as that will
	# cause our unit to be put in a disabled state.
	if grep -q openafs "$MODULEROOT/modules.dep"; then
	    exit $status
	else
	    do_nothing_env
	    exit 0
	fi
    fi
fi

# Determine which afsd options to use.  /etc/openafs/afs.conf contains the
# settings that are checked here.
if [ -z "$OPTIONS" ] || [ "$OPTIONS" = "AUTOMATIC" ] ; then
    AFSD_OPTIONS="$VERBOSE"
else
    AFSD_OPTIONS="$OPTIONS $VERBOSE"
fi

# These variables are from /etc/openafs/afs.conf.client and are managed
# automatically by debconf.
case "$AFS_AFSDB" in
[Yy][Ee][Ss]|[Tt][Rr][Uu][Ee])
    AFSD_OPTIONS="$AFSD_OPTIONS -afsdb"
esac
case "$AFS_DYNROOT" in
[Yy][Ee][Ss]|[Tt][Rr][Uu][Ee])
    AFSD_OPTIONS="$AFSD_OPTIONS -dynroot"
    ;;
[Ss][Pp][Aa][Rr][Ss][Ee])
    AFSD_OPTIONS="$AFSD_OPTIONS -dynroot-sparse"
esac
case "$AFS_FAKESTAT" in
[Yy][Ee][Ss]|[Tt][Rr][Uu][Ee])
    AFSD_OPTIONS="$AFSD_OPTIONS -fakestat"
esac

case "$AFS_CRYPT" in
[Yy][Ee][Ss]|[Tt][Rr][Uu][Ee])
    AFS_SETCRYPT=on
    ;;
*)
    AFS_SETCRYPT=off
esac

# Generate an EnvironmentFile for use by systemd.
cat > /var/cache/openafs-client/openafs-client.env <<EOF
AFSD_ARGS=${AFSD_OPTIONS}
AFS_SETCRYPT=${AFS_SETCRYPT}
AFS_SYSNAME=${AFS_SYSNAME}
KMOD=openafs
EOF
